package util;

import com.google.gson.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Rafael Ortiz
 */
public class Utilidades {

    private static String token = "";

    public static String getToken() {
        return token;
    }

    private String callRestWSByGet(String url) throws IOException {
        URL urlws = new URL(url);
        URLConnection uc = urlws.openConnection();
        uc.connect();
        //Creamos el objeto con el que vamos a leer
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
        String inputLine;
        String contenido = "";
        while ((inputLine = in.readLine()) != null) {
            contenido += inputLine + "\n";
        }
        in.close();
        return (contenido);
    }

    private String callRestWSByPost(String url) throws IOException {
        URL parURL = new URL(url);

        HttpURLConnection urlConnection = (HttpURLConnection) parURL.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        urlConnection.setRequestProperty("Content-type", "application/x-www-form-urlenCcoded");
        urlConnection.setAllowUserInteraction(true);
        urlConnection.connect();

        StringBuilder sb = new StringBuilder();
        InputStream in = ((HttpURLConnection) urlConnection).getInputStream();
        int length = urlConnection.getContentLength();
        for (int n = 0; n < length; n++) {
            sb.append((char) in.read());
        }
        return sb.toString();
    }

    public String login(String mail, String password) {
        try {
            String url = "http://localhost:8080/project/webresources/user/login?mail=" + mail + "&password=" + password;
            return parseadorCodigoGson(callRestWSByPost(url));
        } catch (Exception e) {
            return "-500";
        }
    }

    public String registrar(String mail, String password) {
        try {
            String url = "http://localhost:8080/project/webresources/user/register?mail=" + mail + "&password=" + password;
            return parseadorCodigoGsonRegistro(callRestWSByPost(url));
        } catch (Exception e) {
            return "-500";
        }
    }
   

    public String servicioWifi(String distrito) {
        try {
            distrito = distrito.replace(" ", "%20");
            String url = "http://localhost:8080/project/webresources/servicioswifi/serwifi?district=" + distrito+"&latitude=&longitude=&distance="+ "&token=" + token;
            String result = callRestWSByGet(url);
            return result;
        } catch (Exception e) {
            return "-500";
        }

    }

    public String servicioInmobiliaria(String element, String values) {
        try {
            String url = "http://localhost:8080/project/webresources/inmobiliaria/find?element=" + element + "&value=" + values + "&token=" + token;
            String result = callRestWSByGet(url);
            return result;
        } catch (Exception e) {
            return "-500";
        }
    }
    
    public String servicioTiempo(String ciudad){
        try{
            String url = "http://localhost:8080/project/webresources/tiempo?ciudad="+ciudad+"&token="+token;
            String result = callRestWSByGet(url);
            return result;
        }catch(Exception e){
            return "-500";
        }
    }
    
    private Double convertirCelsius(Double temperatura){
        return temperatura - 273.15;
    }
    
    public String obtenerTiempo(String result){
        String tiempoCiudad;
        try{
            
            JsonParser parser = new JsonParser();
            JsonObject gsonArr = parser.parse(result).getAsJsonObject();
            JsonElement obj = gsonArr.get("obj");
            
            JsonObject gsonalgo = parser.parse(obj.getAsString()).getAsJsonObject();
            
            JsonObject gsoncasi = gsonalgo.getAsJsonObject("main");
            

            Double temperatura = convertirCelsius(gsoncasi.get("temp").getAsDouble());
            Double temperaturaMax = convertirCelsius(gsoncasi.get("temp_max").getAsDouble());
            Double temperaturaMin = convertirCelsius(gsoncasi.get("temp_min").getAsDouble());
            String ciudad = gsonalgo.get("name").getAsString();
            int temperaturaI = temperatura.intValue();
            int temperaturaMaxI = temperaturaMax.intValue();
            int temperaturaMinI = temperaturaMin.intValue();
            tiempoCiudad = "Ciudad: "+ciudad+" Temperatura maxima: "+temperaturaMaxI+" Temperatura minima: "+temperaturaMinI;
            return tiempoCiudad;
        }catch(JSONException e){
            e.printStackTrace();
            return "Error de parseo, vuelva a intentarlo";
        }
    }
    
    public ArrayList<String> obtenerLugares(String result){
        ArrayList resultado = new ArrayList<String>();  
        try{
            JSONArray array = new JSONObject(result).getJSONArray("obj");
            if(array.length() == 0){
                resultado.add("No se han encontrado datos");
                return resultado;
            }else{
                for(int i = 0; i < array.length(); i++){
                    JSONObject obj = array.getJSONObject(i);
                    String nombre = obj.getString("title");
                    resultado.add(nombre);
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public ArrayList<String> obtenerPisos(String result){
        ArrayList resultado = new ArrayList<String>();  
        try{
            JSONArray array = new JSONObject(result).getJSONArray("obj");
            if(array.length() == 0){
                resultado.add("No se han encontrado datos");
                return resultado;
            }else{
                for(int i = 0; i < array.length(); i++){
                    JSONObject obj = array.getJSONObject(i);
                    int referencia = obj.getInt("id");
                    Date fechaAlta = new Date(obj.getString("fechaAlta"));
                    String tipo = obj.getString("tipo");
                    String provincia = obj.getString("provincia");
                    int superficie = obj.getInt("superficie");
                    int precioVenta = obj.getInt("precioVenta");
                    String vendedor = obj.getString("vendedor");
                    String r = referencia + ","+fechaAlta+","+tipo+","+provincia+","+superficie+","+precioVenta+",";
                    resultado.add(r);
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
        return resultado;
    }
    

    private String parseadorCodigoGson(String result) {
        String error = "-5";
        try {
            JSONObject obj = new JSONObject(result);
            int errorCode = obj.getInt("errorCode");
            String msg = obj.getString("msg");
            if(errorCode==3){
                token = obj.getJSONObject("obj").getString("token");
            }
            error = Integer.toString(errorCode);
            return error;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return error;
    }

    private String parseadorCodigoGsonRegistro(String result) {
        String error = "-5";
        try {
            JSONObject obj = new JSONObject(result);
            int errorCode = obj.getInt("errorCode");
            String msg = obj.getString("msg");
            error = Integer.toString(errorCode);
            return error;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return error;
    }

    public ArrayList<String> obtenerDistritos() {
        ArrayList<String> distritos = new ArrayList<>();
        distritos.add("ARGANZUELA");
        distritos.add("BARAJAS");
        distritos.add("CARABANCHEL");
        distritos.add("CENTRO");
        distritos.add("CHAMARTIN");
        distritos.add("CHAMBERI");
        distritos.add("CIUDAD LINEAL");
        distritos.add("FUENCARRAL-EL PARDO");
        distritos.add("HORTALEZA");
        distritos.add("LATINA");
        distritos.add("MONCLOA-ARAVACA");
        distritos.add("MORATALAZ");
        distritos.add("PUENTE DE VALLECAS");
        distritos.add("RETIRO");
        distritos.add("SALAMANCA");
        distritos.add("SAN BLAS-CANILLEJAS");
        distritos.add("TETUAN");
        distritos.add("USERA");
        distritos.add("VICALVARO");
        distritos.add("VILLA DE VALLECAS");
        distritos.add("VILLAVERDE");
        return distritos;
    }

    public void establecerToken(String i) {
        token = i;
    }

}
