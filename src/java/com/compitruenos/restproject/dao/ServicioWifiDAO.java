package com.compitruenos.restproject.dao;

import com.compitruenos.restproject.controller.Districts;
import com.compitruenos.restproject.controller.Json;
import com.compitruenos.restproject.controller.ServicioWifi;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ServicioWifiDAO extends GenericDAO {
    
    private final String URL_DISTRITOS ="https://datos.madrid.es/portal/site/egob/menuitem.ac61933d6ee3c31cae77ae7784f1a5a0/?vgnextoid=00149033f2201410VgnVCM100000171f5a0aRCRD&format=json&file=0&filename=216619-0-wifi-municipal&mgmtid=c182d9b9d34df410VgnVCM2000000c205a0aRCRD&preview=full&distrito_nombre=";
    private final String URL_COORDENADAS="https://datos.madrid.es/egob/catalogo/216619-0-wifi-municipal.json?latitud=";
    
    public ServicioWifiDAO(Session s, Transaction tx) {
        super(s, tx);
    }
    
    
    public List<ServicioWifi> getDistrito(String nombre) throws Exception{
        if(!comprobarDistrito(nombre))
            throw new Exception("nex");
        JsonParser parser = new JsonParser();
        nombre = nombre.replace(" ", "%20");
        JsonArray gsonArr = parser.parse(Json.readUrl(URL_DISTRITOS + nombre.toUpperCase())).getAsJsonObject().get("@graph").getAsJsonArray();
        
        List<ServicioWifi> result = new ArrayList();
        
        if(gsonArr == null)
                throw new Exception("furl");
        
        for (JsonElement obj : gsonArr) {
            JsonObject gsonObj = obj.getAsJsonObject();
            
            String URL = gsonObj.get("@id").getAsString();
            String title = gsonObj.get("title").getAsString();
            Double latitude = gsonObj.get("location").getAsJsonObject().get("latitude").getAsDouble();
            Double longitude = gsonObj.get("location").getAsJsonObject().get("longitude").getAsDouble();
            
            ServicioWifi sw = new ServicioWifi(URL, title, latitude, longitude);
            
            result.add(sw);
        }
        
        return result;
    }
    
    public List<ServicioWifi> getLatitudeLongitude(String latitude, String longitude, int distance) throws Exception{
        JsonParser parser = new JsonParser();
        JsonArray gsonArr = parser.parse(Json.readUrl(URL_COORDENADAS + latitude + "&longitud=" + longitude + "&distancia=" + distance)).getAsJsonObject().get("@graph").getAsJsonArray();
        
        List<ServicioWifi> result = new ArrayList();
        
        if(gsonArr == null)
                throw new Exception("furl");
        
        for (JsonElement obj : gsonArr) {
            JsonObject gsonObj = obj.getAsJsonObject();
            
            String URL = gsonObj.get("@id").getAsString();
            String title = gsonObj.get("title").getAsString();
            Double latitudee = gsonObj.get("location").getAsJsonObject().get("latitude").getAsDouble();
            Double longitudee = gsonObj.get("location").getAsJsonObject().get("longitude").getAsDouble();
            
            ServicioWifi sw = new ServicioWifi(URL, title, latitudee, longitudee);
            
            result.add(sw);
        }
        
        return result;
    }
    

    

    private boolean comprobarDistrito(String distrito){
        ArrayList distritos = new ArrayList<String>();distritos.add("ARGANZUELA");distritos.add("BARAJAS");distritos.add("CARABANCHEL");distritos.add("CENTRO");distritos.add("CHAMARTIN");distritos.add("CHAMBERI");distritos.add("CIUDAD LINEAL");distritos.add("FUENCARRAL-EL PARDO");distritos.add("HORTALEZA");distritos.add("LATINA");distritos.add("MONCLOA-ARAVACA");distritos.add("MORATALAZ");distritos.add("PUENTE DE VALLECAS");distritos.add("RETIRO");distritos.add("SALAMANCA");distritos.add("SAN BLAS-CANILLEJAS");distritos.add("TETUAN");distritos.add("USERA");distritos.add("VICALVARO");distritos.add("VILLA DE VALLECAS");distritos.add("VILLAVERDE");
        if(distritos.contains(distrito.toUpperCase())){
            return true;
        }
        return false;
    }
}
