package com.compitruenos.restproject.dao;

import com.compitruenos.restproject.controller.Encrypt;
import com.compitruenos.restproject.bbdd.Queries;
import com.compitruenos.restproject.pojo.Token;
import com.compitruenos.restproject.pojo.User;
import java.util.Calendar;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserDAO extends GenericDAO{
    /**
     * Constructor unico, Clase controller del usuario para interpretar los parametros del ws.
     * @param s Sesion actual
     * @param tx transacion actual
     */
    public UserDAO(Session s, Transaction tx) {
        super(s, tx);
    }
    /**
     * Controla el registro de usuarios en el sistema
     * @param mail Mail del usuario solicitante
     * @param password password del usuario solicitante
     * @return -> 0: Fallo al registrar<br>
     * -> -1: usuario ya existente<br>
     * -> 10: Usuario registrado correctamente, pero fallo al linkear los servicios<br>
     * -> -11: Usuario registrado correctamente, ningun servicio para linkear al usuarios<br>
     * -> 1*: Usuario registrado correctamente, *=numero de servicios linkeados al usuario<br>
     */
    public int register(String mail, String password) {
        password = Encrypt.encrypt(password);
        try{
            if(userExist(mail, password) == null){
                User u = new User(mail, password, Calendar.getInstance().getTime(), 0);
                getS().persist(u);
                int num = new ServicesDAO(getS(), getTx()).linkServices(u.getPermission());
                switch(num){
                    case 0:
                        return 10;
                    case -1:
                        return -11;
                    default:
                        return Integer.parseInt("" + 1 + num);
                }
            }else{
                return -1;
            }
        }catch(HibernateException e){
            e.printStackTrace();
            return 0;
        }
    }
    /**
     * Controla el Login de usuarios para aceder al sistema
     * @param mail Mail del usuario entrante
     * @param password Contraseña del usuario entrante
     * @return Puede retornar el Token, en formato Token, obtenido al loguearse. En caso de error devuelve null
     * @throws HibernateException Excepcion al realizar la creacion del token.
     */
    public Token login (String mail, String password) throws HibernateException{
        password = Encrypt.encrypt(password);
        User u = userExist(mail, password);
        if (u != null) {
            TokenDAO tDAO = new TokenDAO(getS(),getTx());
            Token token = tDAO.getToken(u);
            if(token != null){
                if(token.getDateExpiration().after(Calendar.getInstance().getTime()))
                    return token;
                else{
                    Token subT = tDAO.createToken(u, false);
                    token.setDateLogin(subT.getDateLogin());
                    token.setDateExpiration(subT.getDateExpiration());
                    token.setToken(subT.getToken());
                    getS().update(token);
                    return token;
                }
            }
            else{
                token = tDAO.createToken(u, true);
                u.setToken(token);
                getS().update(u);
                return token;
            }
        }
        return null;
    }
    /**
     * Controla si un usuario existe o no
     * @param mail Mail del usuario a comprobar
     * @param password Contraseña dle usuario a comprobar
     * @return Devuelve el usuario, si los datos son correctos, o null en caso de error en los datos pasados.
     */
    public User userExist(String mail, String password){
        Query q = getS().createQuery(Queries.FIND_USER);
        q.setParameter("mail", mail);
        q.setParameter("password", password);
        return (User) q.uniqueResult();
    }
    /**
     * Devuelve el usuario en formato User el cual coincida con el nombre pasado por parametro
     * @param mail Mail del usuario a devolver
     * @return Devuelve el usuario en formato User del mail pasado por parametro.
     */
    public User getUser(String mail){
        Query q = getS().createQuery(Queries.FIND_USER_BY_MAIL);
        q.setParameter("mail", mail);
        return (User) q.uniqueResult();
    }
}
