package com.compitruenos.restproject.dao;

import com.compitruenos.restproject.controller.Excepciones;
import com.compitruenos.restproject.controller.Json;
import com.compitruenos.restproject.controller.Inmobiliaria;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class InmobiliariaDAO extends GenericDAO{
    
    private final String URL = "https://api.sheety.co/e00cddd0-9012-49a8-8ba4-feecf8f74e26";

    public InmobiliariaDAO(Session s, Transaction tx) {
        super(s, tx);
    }
    
    private List<Inmobiliaria> getValueByElement(String element, String valueClient){
        try{
            if(element.equals("operacion"))
                element = "operacióN";
            JsonParser parser = new JsonParser();

            JsonArray gsonArr = parser.parse(Json.readUrl(URL)).getAsJsonArray();

            List<Inmobiliaria> result = new ArrayList();
            
            /*if(gsonArr.get(0).getAsJsonObject().get(element).isJsonNull())
                throw new Excepciones(-19);*/

            for (JsonElement obj : gsonArr) {
                JsonObject gsonObj = obj.getAsJsonObject();
                
                
                if(!gsonObj.get(element).isJsonNull()){
                
                    if(gsonObj.get(element).getAsString().equals(valueClient)){
                        
                        int referencia = gsonObj.get("referencia").getAsInt();
                        Date fechaAlta = new Date(gsonObj.get("fechaAlta").getAsString());
                        String tipo = gsonObj.get("tipo").getAsString();
                        String operacion = gsonObj.get("operacióN").getAsString();
                        String provincia = gsonObj.get("provincia").getAsString();
                        int superficie = gsonObj.get("superficie").getAsInt();
                        int precioVenta = gsonObj.get("precioVenta").getAsInt();
                        Date fechaVenta = null; ;
                        if(!gsonObj.get("fechaVenta").isJsonNull())
                            fechaVenta = new Date(gsonObj.get("fechaVenta").getAsString());
                        String vendedor = null;
                        if(!gsonObj.get("vendedor").isJsonNull())
                            vendedor = gsonObj.get("vendedor").getAsString();

                        Inmobiliaria i = new Inmobiliaria(referencia, fechaAlta, tipo, operacion, provincia, superficie, precioVenta, fechaVenta, vendedor);
                        
                        result.add(i);
                    }
                }
            }
            return result;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public String getAll() throws Exception{
        return Json.readUrl(URL);
    }
    
    public List<Inmobiliaria> getValue(List<String> elements, List<String> values){
        int condiciones = elements.size();//CONTROLA EL NUMERO DE CONDICIONES EXPUESTAS
        List<Inmobiliaria> temporal = new ArrayList();
        for (int i = 0; i < elements.size(); i++) {
            List<Inmobiliaria> temporal2 = getValueByElement(elements.get(i), values.get(i));
            for (Inmobiliaria inmobiliaria : temporal2) {
                temporal.add(inmobiliaria);
            }
        }

        List<Inmobiliaria> resultado = new ArrayList();
        
        int num = temporal.size();
        Inmobiliaria inmobiliaria;
        for (int i = 0; i < num && temporal.size()>0; i++) {
            inmobiliaria = temporal.get(0);
            temporal.remove(0);
            int nume = 0;
            for (Inmobiliaria inmo : temporal) {
                if(inmo.getId() == inmobiliaria.getId()){
                    nume++;
                    temporal.remove(inmobiliaria);
                }
            }
            if(nume == condiciones-1)
                resultado.add(inmobiliaria);
        }
        
        return resultado;
    }
}
