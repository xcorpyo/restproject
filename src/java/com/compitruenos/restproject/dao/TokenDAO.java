package com.compitruenos.restproject.dao;

import com.compitruenos.restproject.controller.Encrypt;
import com.compitruenos.restproject.bbdd.Queries;
import com.compitruenos.restproject.pojo.Token;
import com.compitruenos.restproject.pojo.User;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TokenDAO extends GenericDAO{
    
    public TokenDAO(Session s, Transaction tx) {
        super(s, tx);
    }
    
    public Token createToken(User u, boolean bandera){
        Date d = Calendar.getInstance().getTime();
        d.setMinutes(d.getMinutes() + 30);
        String token = u.getPermission() + "x" + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime()) + "z" + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(d);
        
        Token t = new Token(Encrypt.encrypt(token), Calendar.getInstance().getTime(), d);
        if(bandera)
            getS().persist(t);
        return t;
    }
    
    public Token getToken(User u){
        Query q = getS().createQuery(Queries.GET_TOKEN_BY_USER);
        q.setParameter("id", u.getToken());
        return (Token) q.uniqueResult();
    }
    
    public Token getToken(String token){
        Query q = getS().createQuery(Queries.GET_TOKEN_BY_TOKEN);
        q.setParameter("token", token);
        return (Token) q.uniqueResult();
    }
}
