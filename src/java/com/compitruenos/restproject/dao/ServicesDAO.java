package com.compitruenos.restproject.dao;

import com.compitruenos.restproject.bbdd.Queries;
import com.compitruenos.restproject.pojo.Services;
import com.compitruenos.restproject.pojo.ServicesHasUser;
import com.compitruenos.restproject.pojo.User;
import java.util.Calendar;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ServicesDAO extends GenericDAO {
    /**
     * Constructor unico que controla los servicios
     * @param s Sesion
     * @param tx Transacion
     */
    public ServicesDAO(Session s, Transaction tx) {
        super(s, tx);
    }
    /**
     * Funcion que crea un servicio y devuelve un valor dependiendo de la respuesta obtenida
     * @param nameNombre del servicio a crear
     * @return 0 -> Fallo al registrar<br>
     * -> -1: Servicio ya existente<br>
     * -> 10: Servicio registrado correctamente, pero fallo al linkear los servicios<br>
     * -> -11: Servicio registrado correctamente, ningun usuario para linkear servicios<br>
     * -> 1*: Servicio registrado correctamente, *=numero de usuarios linkeados al servicio<br>
     */
    public int createService(String name){
        try{
            if(serviceExist(name) == null){
                Services s = new Services(name, Calendar.getInstance().getTime(), 0);
                getS().persist(s);
                int num = linkServices(s.getPermission());
                switch(num){
                    case 0:
                        return 10;
                    case -1:
                        return -11;
                    default:
                        return Integer.parseInt("" + 1 + num);
                }
            }else{
                return -1;
            }
        }catch(HibernateException e){
            e.printStackTrace();
            return 0;
        }
    }
    
    /**
     * Linkea los servicios pudiendo dar aceso a los usuarios con un nivel de permiso a sus servicios, con mismo nivel de permiso
     * @param permission Nivel de permiso solicitado para linkear los servicios
     * @return 0 -> Error generico<br>
     * -1 -> NINGUN SERVICIO O USUARIO DISPONIBLE PARA LINKEAR<br>
     * -> * -> Numero de servicios linkeados correctamente
     */
    public int linkServices(int permission){//EL FLAG CONTROLA SI SE MANDA DESDE UN SERVICIO O DESDE UN USUARIO PARA CAMBIAR LA QUERY. TRUE SERA SERVICIO Y FALSE SERIA USUARIO
        int num = 0;
        Query q = getS().createQuery(Queries.GET_ALL_SERVICES_BY_PERMISSION);
        q.setParameter("permission", permission);
        List<Services> ls = q.list();

        q = getS().createQuery(Queries.GET_ALL_SERVICES_BY_PERMISSION);
        q.setParameter("permission", permission);
        List<User> lu = q.list();

        if(lu != null && ls != null){
            for (User u : lu) {
                for (Services s : ls) {
                    num++;
                    ServicesHasUser shu = new ServicesHasUser(s, u, Calendar.getInstance().getTime());
                    getS().persist(shu);
                }
            }
        }
        else{
            num = -1;
        }
        return num;
    }
    /**
     * Comprueba que existe un servicio, pasandole como parametro el nombre del servicio
     * @param name Nombre del servicio
     * @return Devuelve el servicio enocntrado o null si no existe
     */
    private Services serviceExist(String name){
        Query q = getS().createQuery(Queries.FIND_SERVICES);
        q.setParameter("name", name);
        return (Services) q.uniqueResult();
    }
    /**
     * Comprueba que exista una relacion de aceso entre un servicio y un usuario pasado por parametro
     * @param service Servicio a comprobar, en formato String
     * @param user usuario a comprobar, en formato String
     * @return Devuelve el enlace encontrado o, si no se encuentra, devuelve null
     * @throws HibernateException Error con BBDD
     * @throws Exception Error generico
     */
    public ServicesHasUser serviceUserExist (String service, String user) throws HibernateException, Exception{
        Services s = serviceExist(service);
        User u = new UserDAO(getS(), getTx()).getUser(user);
        
        return serviceUser(s, u);
    }
    /**
     * Devuelve el link de servicios y usuarios que se pasa por parametro
     * @param service Servici en formato Services
     * @param user Usuario en formate User
     * @return Devuelve el link en formato ServiceHasUser o null en caso de no existir
     * @throws HibernateException Error con BBDD
     * @throws Exception Error generico
     */
    private ServicesHasUser serviceUser (Services service, User user) throws HibernateException, Exception{
        Query q = getS().createQuery(Queries.GET_LINK_SERVICE);
        q.setParameter("user", user);
        q.setParameter("service", service);
        
        return (ServicesHasUser) q.uniqueResult();
    }
}
