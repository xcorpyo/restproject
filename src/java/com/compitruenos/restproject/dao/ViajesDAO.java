package com.compitruenos.restproject.dao;

import com.compitruenos.restproject.controller.Json;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.LinkedHashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ViajesDAO extends GenericDAO {
    private final static String URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric";
    private static final String KEY = "&key=AIzaSyBK5rvS-N8vDNCza9xw97yZkMklFnfBGjA";

    public ViajesDAO(Session s, Transaction tx) {
        super(s, tx);
    }
    
    public Map<String, String> calcularDirecion(String origen, String destino){
        origen = origen.replace(" ", "%20");
        destino = destino.replace(" ", "%20");
        String URL = this.URL + "&origins=" + origen + "&destinations=" + destino + KEY;
        
        Map<String, String> fin = new LinkedHashMap();
        
        try{
            JsonParser parser = new JsonParser();

            JsonObject gsonO = parser.parse(Json.readUrl(URL)).getAsJsonObject();

            origen = origen.replace("%20", " ");
            destino = destino.replace("%20", " ");
            fin.put("origen", origen);
            fin.put("destino", destino);
            
            int distancia = gsonO.getAsJsonArray("rows").get(0).getAsJsonObject().getAsJsonArray("elements").get(0).getAsJsonObject().getAsJsonObject("distance").get("value").getAsInt();
            int tiempo = gsonO.getAsJsonArray("rows").get(0).getAsJsonObject().getAsJsonArray("elements").get(0).getAsJsonObject().getAsJsonObject("duration").get("value").getAsInt();
            double coste = (distancia * 0.001) + (tiempo * 0.0017);
            
            fin.put("distancia", "" + distancia);
            fin.put("duracion", "" + tiempo);
            fin.put("coste", "" + coste);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        return fin;
    }
}
