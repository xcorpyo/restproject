package com.compitruenos.restproject.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class GenericDAO {
    private Session s;
    private Transaction tx;
    /**
     * Constructor solo con sesion para funcionar como generico en un controllador
     * @param s Sesion
     */
    public GenericDAO(Session s) {
        this.s = s;
    }
    /**
     * Constructor con todos los parametros. Para funcionar como generico en un controllador
     * @param s Sesion
     * @param tx Transacion
     */
    public GenericDAO(Session s, Transaction tx) {
        this.s = s;
        this.tx = tx;
    }
    
    public Session getS(){
        return s;
    }    
    public Transaction getTx(){
        return tx;
    }
}
