package com.compitruenos.restproject.dao;

import com.compitruenos.restproject.controller.Json;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TiempoDAO extends GenericDAO{
    
    public static final  String URL = "http://api.openweathermap.org/data/2.5/weather?APPID=509f82033a7460427eacbb2dedb68099";
    
    public TiempoDAO(Session s, Transaction tx) {
        super(s, tx);
    }
    
    public String locLatiLon(double latitud, double longitud) throws Exception{
        return Json.readUrl(URL + "&lat=" + latitud + "&lon=" + longitud);
    }
    
    public String locZip(String zip) throws Exception{
        return Json.readUrl(URL + "&zip=" + zip + ",es");
    }
    
    public String locCiudad(String ciudad) throws Exception{
        return Json.readUrl(URL + "&q=" + ciudad);
    }
}
