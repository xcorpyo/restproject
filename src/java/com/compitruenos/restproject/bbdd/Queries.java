package com.compitruenos.restproject.bbdd;

public interface Queries {
    
    public static final String FIND_USER = "FROM User WHERE mail = :mail and password = :password and (dateExpiration is null or dateExpiration > now())";
    public static final String FIND_USER_BY_MAIL = "FROM User WHERE mail = :mail and (dateExpiration is null or dateExpiration > now())";
    public static final String FIND_SERVICES = "FROM Services WHERE nameServices = :name and (dateEnd is null or dateEnd > now())";
    public static final String GET_TOKEN_BY_USER = "from Token where id = :id and dateExpiration > now()";
    public static final String GET_TOKEN_BY_TOKEN = "from Token where token = :token and dateExpiration > now()";
    public static final String GET_LINK_SERVICE = "from ServicesHasUser where services_idServices = :service and user_idUsuario = :user and (dateEnd is null or dateEnd > now())";
    
    public static final String GET_ALL_USER_BY_PERMISSION = "from User where (dateExpiration is null or dateExpiration > now()) and permission = :permission";
    public static final String GET_ALL_SERVICES_BY_PERMISSION = "from Services where (dateEnd is null or dateEnd > now()) and permission = :permission";
}