package com.compitruenos.restproject.controller;

public class CheckIP {
    
    static final String localURLipv6 = "0:0:0:0:0:0:0:1";
    static final String localURLipv4 = "127.0.0.1";
    
    public static boolean checkIP(javax.servlet.http.HttpServletRequest hsr){
        if(!localURLipv6.equals(hsr.getRemoteAddr()) && !localURLipv4.equals(hsr.getRemoteAddr())){
            return false;
        }
        return true;
    }
}
