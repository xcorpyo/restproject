package com.compitruenos.restproject.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Json {
    public static String readUrl(String urlString) throws Exception {
        
        BufferedReader reader = null;
        
        try {
            URLConnection openConnection = new URL(urlString).openConnection();
            openConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
            reader = new BufferedReader(new InputStreamReader(openConnection.getInputStream()));
            StringBuilder buffer = new StringBuilder();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1){
                buffer.append(chars, 0, read); 
            }
            return buffer.toString();
        }
        finally {
            if (reader != null)
                reader.close();
        }
    }
}
