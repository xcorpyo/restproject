package com.compitruenos.restproject.controller;

public class ServicioWifi {
    private String URL;
    private String title;
    private Double latitude;
    private Double longitude;

    public ServicioWifi(String URL, String title, Double latitude, Double longitude) {
        this.URL = URL;
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
