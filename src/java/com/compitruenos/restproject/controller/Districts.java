package com.compitruenos.restproject.controller;

public class Districts {
    private int perimetro;
    private int codigo;
    private String nombre;
    private String nombrePublico;
    private int barrios;
    private int superficie;

    public Districts(int perimetro, int codigo, String nombre, String nombrePublico, int bariios, int superficie) {
        this.perimetro = perimetro;
        this.codigo = codigo;
        this.nombre = nombre;
        this.nombrePublico = nombrePublico;
        this.barrios = bariios;
        this.superficie = superficie;
    }

    public int getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(int perimetro) {
        this.perimetro = perimetro;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombrePublico() {
        return nombrePublico;
    }

    public void setNombrePublico(String nombrePublico) {
        this.nombrePublico = nombrePublico;
    }

    public int getBarrios() {
        return barrios;
    }

    public void setBarrios(int bariios) {
        this.barrios = bariios;
    }

    public int getSuperficie() {
        return superficie;
    }

    public void setSuperficie(int superficie) {
        this.superficie = superficie;
    }
}
