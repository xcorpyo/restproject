package com.compitruenos.restproject.controller;

import java.util.Date;

public class Inmobiliaria {
    private int id;
    private Date fechaAlta;
    private String tipo;
    private String operacion;
    private String provincia;
    private int superficie;
    private int precioVenta;
    private Date fechaVenta;
    private String vendedor;

    public Inmobiliaria(int id, Date fechaAlta, String tipo, String operacion, String provincia, int superficie, int precioVenta, Date fechaVenta, String vendedor) {
        this.id = id;
        this.fechaAlta = fechaAlta;
        this.tipo = tipo;
        this.operacion = operacion;
        this.provincia = provincia;
        this.superficie = superficie;
        this.precioVenta = precioVenta;
        this.fechaVenta = fechaVenta;
        this.vendedor = vendedor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public int getSuperficie() {
        return superficie;
    }

    public void setSuperficie(int superficie) {
        this.superficie = superficie;
    }

    public int getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(int precioVenta) {
        this.precioVenta = precioVenta;
    }

    public Date getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(Date fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }
}
