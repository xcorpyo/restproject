package com.compitruenos.restproject.controller;

import com.compitruenos.restproject.dao.TokenDAO;
import com.compitruenos.restproject.ws.GenericWS;

public class CheckToken extends GenericWS {
    public boolean checkToken(String token){
        try{
            connect();
            if(new TokenDAO(getS(), getTx()).getToken(token) == null){
                return false;
            }
            disconnect();
            return true;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
