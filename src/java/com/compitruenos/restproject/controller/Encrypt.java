package com.compitruenos.restproject.controller;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class Encrypt {
    /**
     * Clase publica que devuelve una clave encriptada
     * @param msg Texto a encriptar
     * @return Devuelve el texto pasado por parametro encriptado
     */
    public static String encrypt(String msg) {
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(msg.getBytes("UTF-8"));
            return byteToHex(crypt.digest());
        }
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            return null;
        }
    }
    /**
     * Clase que convierte de byte a Hexadecimal
     * @param hash Numeros que se desean convertir
     * @return Devuelve el valor pasado por parametro seteado a Hexadecimal
     */
    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
