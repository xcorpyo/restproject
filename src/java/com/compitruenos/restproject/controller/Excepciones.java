package com.compitruenos.restproject.controller;

import com.compitruenos.restproject.rb.Spanish;

public class Excepciones extends Exception {
    
    private int codigo;

    public Excepciones(int codigo) {
        super(Spanish.getMsg(codigo));
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
}
