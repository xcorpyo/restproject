package com.compitruenos.restproject.rb;

public class ResponseBean {
    /**
     * Controla el error en un codigo privado del programa
     */
    private int errorCode;
    /**
     * Expone el mensage en texto para la interpretacion del usuario
     */
    private String msg;
    /**
     * Devuelve un objeto, parseado en GSON para interpretacion del cliente
     */
    private Object obj;

    /**
     * Contructor vacio para ir añadiendo parametros
     */
    public ResponseBean() {
    }
    /**
     * Constructor que inlcuye codigo de error y setea el mensage parseando ese codigo segun inscritos en el programa
     * @param errorCode Codigo de error proporcionado
     */
    public ResponseBean(int errorCode) {
        this.errorCode = errorCode;
        msg = Spanish.getMsg(errorCode);
    }
    /**
     * Constructor con con error y objeto ya seteado
     * @param errorCode Codigo de error proporcionado
     * @param obj Objeto que se incluye para la interpretacion en GSON del cliente
     */
    public ResponseBean(int errorCode, Object obj) {
        this.errorCode = errorCode;
        this.obj = obj;
    }
    /**
     * Constructor con error y mensaje ya seteado. Siendo este mensage el introducido y no el generico del sistema
     * @param errorCode Codigo de error
     * @param msg Mensage para interpretacion del usuario en texto
     */
    public ResponseBean(int errorCode, String msg) {
        this.errorCode = errorCode;
        this.msg = msg;
    }
    /**
     * Constructor con todos los parametros seteados a elecion de la llamada
     * @param errorCode Codigo de error a mostrar
     * @param msg Mensaje en texto plano para la interpretacion del usuario
     * @param obj Objeto parseado en GSON para la interpretacion por el cliente
     */
    public ResponseBean(int errorCode, String msg, Object obj) {
        this.errorCode = errorCode;
        this.msg = msg;
        this.obj = obj;
    }

    public int getErrorCode() {
        return errorCode;
    }
    /**
     * cambia o introduce el codigo de error a la clase. Auto setea el Mensage dependiendo del codigo
     * @param errorCode Codigo de rror
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
        setMsg(Spanish.getMsg(errorCode));
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
    
    
}