package com.compitruenos.restproject.rb;

public class Spanish {
    public static String getMsg(int errorCode){
        String st;
        switch(errorCode){
            /**
             * EJECUCION CORRECTA
             */
            case 5:
                st = "Servicio enlazado correctamente";
                break;
            case 4:
                st = "Servicio añadidio Correctamente";
                break;
            case 3:
                st = "Usuario correcto";
                break;
            case 2:
                st = "Usuario registrado correctamente";
                break;
            case 1:
                st = "Servicio acesible";
                break;
            /**
             * OK GENERICO
             */
            case 0:
                st = "OK";
                break;
            /**
             * FALLOS
             */
            case -1:
                st = "Faltan Parametros";
                break;
            case -2:
                st = "Usuario Existente";
                break;
            case -3:
                st = "Parametros en formato incorrecto";
                break;
            case -4:
                st = "Error Hibernate";
                break;
            case -5:
                st = "Error Generico";
                break;
            case -6:
                st = "Usuario o Contraseña incorrectos";
                break;
            case -7:
                st = "Servicio inacesible";
                break;
            case -8:
                st = "Fallo al registrar";
                break;
            case -9:
                st = "Enlace ya existente";
                break;
            case -10:
                st = "Token inexistente";
                break;
            case -11:
                st = "Imposible realizar conexion";
                break;  
            case -12:
                st = "Servicio ya existente";
                break;
            case -13:
                st = "Fallo al linkear servicios";
                break;
            case -14:
                st = "Ningun servicio o usuario disponible para linkear";
                break;
            case -15:
                st = "Fallo al añadir servicio";
                break;
            case -16:
                st = "Cliente es distinta de localhost";
                break;
            case -17:
                st = "El distrito no existe";
                break;
            case -18:
                st = "Error durante la recogida de datos";
                break;
            case -19:
                st = "No se encuentra el elemento selecionado";
                break;
            case -20:
                st = "Longitud de elementos y variables diferente";
                break;
            case -21:
                st = "Parametro en elemento repetido";
                break;
            case -22:
                st = "Token invalido o caducado";
                break;
            /**
             * ERRORES NO CODIFICADOS
             */
            default:
                st = "Error no codificado";
        }
        return st;
    }
}
