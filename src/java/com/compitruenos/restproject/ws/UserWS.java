package com.compitruenos.restproject.ws;

import com.compitruenos.restproject.dao.UserDAO;
import com.compitruenos.restproject.pojo.Token;
import com.compitruenos.restproject.rb.ResponseBean;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.hibernate.HibernateException;

@Path("user")
public class UserWS extends GenericWS{
    
    @POST
    @Path("/register")
    @Produces(MediaType.APPLICATION_JSON)
    public String add(@QueryParam("mail") String mail, @QueryParam("password") String password) {
        
        ResponseBean rb = new ResponseBean();
        
        try {
            if (mail == null || mail.trim().isEmpty() || password == null || password.trim().isEmpty()) {
                rb.setErrorCode(-1);
            }else {
                mail = mail.trim();
                password = password.trim();
                
                connect();
                UserDAO uDAO = new UserDAO(getS(), getTx());
                
                switch(uDAO.register(mail, password)){
                    case 0:
                        rb.setErrorCode(-8);
                        break;
                    case -1:
                        rb.setErrorCode(-2);
                        break;
                    case 10:
                        rb.setErrorCode(2);
                        rb.setObj(new ResponseBean(-13));
                        break;
                    case -11:
                        rb.setErrorCode(2);
                        rb.setObj(new ResponseBean(-14));
                        break;
                    default:
                        rb.setErrorCode(2);
                        rb.setObj(new ResponseBean(5));
                        break;
                }

                disconnect();
            }
        }
        catch (NumberFormatException e) {
            rb.setErrorCode(-3);
        }
        catch (HibernateException e) {
            rb.setErrorCode(-4);
            e.printStackTrace();
        }
        catch (java.net.ConnectException e) {
            rb.setErrorCode(-11);
            e.printStackTrace();
        }
        catch (Exception e) {
            rb.setErrorCode(-5);
            e.printStackTrace();
        }
        
        return getJson().toJson(rb);
    }
    
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public String login(@QueryParam("mail") String mail, @QueryParam("password") String password) {
        
        ResponseBean rb = new ResponseBean();
        
        try {
            if (mail == null || mail.trim().isEmpty() || password == null || password.trim().isEmpty()) {
                rb.setErrorCode(-1);
            }else {
                mail = mail.trim();
                password = password.trim();
                
                connect();
                UserDAO uDAO = new UserDAO(getS(), getTx());
                
                Token t = uDAO.login(mail, password);
                if (t != null){
                    rb.setErrorCode(3);
                    Token t2 = new Token();
                    t2.setToken(t.getToken());
                    t2.setUsers(null);
                    rb.setObj(t2);
                }
                else{
                    rb.setErrorCode(-6);
                }

                disconnect();
            }
        }
        catch (NumberFormatException e) {
            rb.setErrorCode(-3);
        }
        catch (HibernateException e) {
            rb.setErrorCode(-4);
            e.printStackTrace();
        }
        catch (java.net.ConnectException e) {
            rb.setErrorCode(-11);
            e.printStackTrace();
        }
        catch (Exception e) {
            rb.setErrorCode(-5);
            e.printStackTrace();
        }
        
        return getJson().toJson(rb);
    }
}