package com.compitruenos.restproject.ws;

import com.compitruenos.restproject.controller.CheckIP;
import com.compitruenos.restproject.controller.CheckToken;
import com.compitruenos.restproject.controller.ServicioWifi;
import com.compitruenos.restproject.dao.ServicioWifiDAO;
import com.compitruenos.restproject.rb.ResponseBean;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.hibernate.HibernateException;

@Path("servicioswifi")
public class ServicioWifiWS extends GenericWS {

    @Context
    //private UriInfo context;
    private javax.servlet.http.HttpServletRequest hsr;

    public ServicioWifiWS() {
    }

    @GET
    @Path("/serwifi")
    @Produces(MediaType.APPLICATION_JSON)
    public String findInmo(@QueryParam("district") String district, @QueryParam("latitude") String latitude, @QueryParam("longitude") String longitude, @QueryParam("distance") String distance, @QueryParam("token") String token) {
        ResponseBean rb = new ResponseBean();

        if (!new CheckToken().checkToken(token)) {
            rb.setErrorCode(-22);
            return getJson().toJson(rb);
        }

        if (!CheckIP.checkIP(hsr)) {
            rb.setErrorCode(-16);
            return getJson().toJson(rb);
        }

        try {
            if (district != null && !district.trim().isEmpty()) {
                if (latitude == null || latitude.trim().isEmpty()) {
                    if (longitude == null || longitude.trim().isEmpty()) {
                        if (distance == null || distance.trim().isEmpty()) {
                            ServicioWifiDAO swDAO = new ServicioWifiDAO(getS(), getTx());

                            List<ServicioWifi> lsw = swDAO.getDistrito(district);
                            if (lsw != null) {
                                rb.setErrorCode(0);
                                rb.setObj(lsw);
                            } else {
                                rb.setErrorCode(-5);
                            }

                        } else {
                            rb.setErrorCode(-3);
                            return getJson().toJson(rb);
                        }
                    } else {
                        rb.setErrorCode(-3);
                        return getJson().toJson(rb);
                    }
                } else {
                    rb.setErrorCode(-3);
                    return getJson().toJson(rb);
                }
            } else if (latitude != null && !latitude.trim().isEmpty() && longitude != null && !longitude.trim().isEmpty() && distance != null && !distance.trim().isEmpty()) {
                int distancia = Integer.parseInt(distance);

                /*Aqui estableceremos la distancia minima*/
                if (distancia > 100) {

                    ServicioWifiDAO swDAO = new ServicioWifiDAO(getS(), getTx());
                    List<ServicioWifi> lsw = swDAO.getLatitudeLongitude(latitude, longitude, distancia);
                    if (lsw != null) {
                        rb.setErrorCode(-0);
                        rb.setObj(lsw);
                        return getJson().toJson(rb);
                    } else {
                        rb.setErrorCode(-5);
                    }
                } else {
                    rb.setErrorCode(-3);
                    return getJson().toJson(rb);
                }

            } else {
                rb.setErrorCode(-1);
                return getJson().toJson(rb);
            }
        } catch (NumberFormatException e) {
            rb.setErrorCode(-3);
        } catch (HibernateException e) {
            rb.setErrorCode(-4);
            e.printStackTrace();
        } catch (java.net.ConnectException e) {
            rb.setErrorCode(-11);
            e.printStackTrace();
        } catch (Exception e) {
            if (e.getMessage().equals("nex")) {
                rb.setErrorCode(-17);
                e.printStackTrace();
            } else if (e.getMessage().equals("furl")) {
                rb.setErrorCode(-18);
                e.printStackTrace();
            } else {
                rb.setErrorCode(-5);
                e.printStackTrace();
            }
        }

        return getJson()
                .toJson(rb);
    }

}
