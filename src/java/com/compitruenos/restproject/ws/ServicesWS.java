package com.compitruenos.restproject.ws;

import com.compitruenos.restproject.dao.ServicesDAO;
import com.compitruenos.restproject.rb.ResponseBean;
import javax.ws.rs.GET;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.hibernate.HibernateException;

@Path("services")
public class ServicesWS extends GenericWS {

    @Path("/add")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String add(@QueryParam("name") String name) {
        
        ResponseBean rb = new ResponseBean();
        
        try {
            if (name == null || name.trim().isEmpty()) {
                rb.setErrorCode(-1);
            }else {
                name = name.trim();
                
                connect();
                ServicesDAO sDAO = new ServicesDAO(getS(), getTx());
                
                switch(sDAO.createService(name)){
                    case 0:
                        rb.setErrorCode(-15);
                        break;
                    case -1:
                        rb.setErrorCode(-12);
                        break;
                    case 10:
                        rb.setErrorCode(4);
                        rb.setObj(new ResponseBean(-13));
                        break;
                    case -11:
                        rb.setErrorCode(4);
                        rb.setObj(new ResponseBean(-14));
                        break;
                    default:
                        rb.setErrorCode(4);
                        rb.setObj(new ResponseBean(5));
                        break;
                }

                disconnect();
            }
        }
        catch (NumberFormatException e) {
            rb.setErrorCode(-3);
        }
        catch (HibernateException e) {
            rb.setErrorCode(-4);
            e.printStackTrace();
        }
        catch (java.net.ConnectException e) {
            rb.setErrorCode(-11);
            e.printStackTrace();
        }
        catch (Exception e) {
            rb.setErrorCode(-5);
            e.printStackTrace();
        }
        
        return getJson().toJson(rb);
    }
    
    @GET
    @Path("/accessservice")
    @Produces(MediaType.APPLICATION_JSON)
    public String accessService(@QueryParam("user") String user, @QueryParam("nameService") String nameService) {
        
        ResponseBean rb = new ResponseBean();
        
        try {
            if (user == null || user.trim().isEmpty() || nameService == null || nameService.trim().isEmpty()) {
                rb.setErrorCode(-1);
            }else {
                user = user.trim();
                nameService = nameService.trim();
                
                connect();
                ServicesDAO sDAO = new ServicesDAO(getS(), getTx());
                
                if(sDAO.serviceUserExist(user, nameService) != null){
                    rb.setErrorCode(1);
                }
                else{
                    rb.setErrorCode(-7);
                }

                disconnect();
            }
        }
        catch (NumberFormatException e) {
            rb.setErrorCode(-3);
        }
        catch (HibernateException e) {
            rb.setErrorCode(-4);
            e.printStackTrace();
        }
        catch (java.net.ConnectException e) {
            rb.setErrorCode(-11);
            e.printStackTrace();
        }
        catch (Exception e) {
            rb.setErrorCode(-5);
            e.printStackTrace();
        }
        
        return getJson().toJson(rb);
    }
}
