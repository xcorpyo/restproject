package com.compitruenos.restproject.ws;

import com.compitruenos.restproject.controller.CheckIP;
import com.compitruenos.restproject.controller.CheckToken;
import com.compitruenos.restproject.controller.Excepciones;
import com.compitruenos.restproject.controller.Inmobiliaria;
import com.compitruenos.restproject.dao.InmobiliariaDAO;
import com.compitruenos.restproject.rb.ResponseBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.hibernate.HibernateException;

@Path("inmobiliaria")
public class InmobiliariaWS extends GenericWS{
    
    @Context
    //private UriInfo context;
    private javax.servlet.http.HttpServletRequest hsr;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String see(@QueryParam("token") String token){
        
        ResponseBean rb = new ResponseBean();
        
        try {
            
            if(token == null || token.trim().isEmpty())
                throw new Excepciones (-1);
            if(!new CheckToken().checkToken(token))
                throw new Excepciones (-22);
            if(!CheckIP.checkIP(hsr))
                throw new Excepciones(-16);
            
            InmobiliariaDAO iDAO = new InmobiliariaDAO(getS(), getTx());
            String st = iDAO.getAll();

            if(st != null){
                rb.setErrorCode(0);
                rb.setObj(st);
            }
            else
                rb.setErrorCode(-5);
        }
        catch (NumberFormatException e) {
            rb.setErrorCode(-3);
        }
        catch (HibernateException e) {
            rb.setErrorCode(-4);
            e.printStackTrace();
        }
        catch (java.net.ConnectException e) {
            rb.setErrorCode(-11);
            e.printStackTrace();
        }
        catch (Exception e) {
            rb.setErrorCode(-5);
            e.printStackTrace();
        }
        
        return getJson().toJson(rb);
    }
    
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    public String find(@QueryParam("element") String element, @QueryParam("value") String value, @QueryParam("token") String token) {
        
        ResponseBean rb = new ResponseBean();
        
        try{
            
            if(token == null || token.trim().isEmpty())
                throw new Excepciones (-1);
            if(!new CheckToken().checkToken(token))
                throw new Excepciones (-22);
            if(!CheckIP.checkIP(hsr))
                throw new Excepciones(-16);
            
            if(element == null || element.trim().isEmpty() || value == null || value.trim().isEmpty())//COMPROBAMOS QUE VENGAN BIEN LAS VARIABLES ENTERAS
                throw new Excepciones(-1);
            Scanner sc;
            List<String> elements = new ArrayList();//LAS SEPARAMOS Y DELIMITAMOS EN LIST
            sc = new Scanner(element);
            sc.useDelimiter(",");
            while(sc.hasNext()){
                elements.add(sc.next());
            }
            List<String> values = new ArrayList();
            sc = new Scanner(value);
            sc.useDelimiter(",");
            while(sc.hasNext()){
                values.add(sc.next());
            }
            
            if(elements.size() == 0 || values.size() == 0)//COMPROBAMOS QUE CONTENGAN VALORES Y QUE SEAN DE LA MISMA EXTENSION
                throw new Excepciones(-5);
            else if(elements.size() != values.size())
                throw new Excepciones(-20);
            
            ArrayList<String> comp = new ArrayList();//COMPROBAREMOS QUE LOS VALORES DEL LIST NO ESTEN VACIOS O REPETIDOS, EN EL CASO DE ELEMENTS
            for (String st : elements) {
                if(st == null || st.trim().isEmpty())
                    throw new Excepciones(-1);
                else if(comp.contains(st))
                    throw new Excepciones(-21);
            }
            comp = new ArrayList();
            for (String st : values) {
                if(st == null || st.trim().isEmpty())
                    throw new Excepciones(-1);
            }
            /*FIN DE LAS COMPROBACIONES*/
            
            InmobiliariaDAO iDAO = new InmobiliariaDAO(getS(), getTx());
            
            List<Inmobiliaria> resultado = iDAO.getValue(elements, values);
            if(resultado != null && resultado.size() != 0){
                rb.setErrorCode(0);
                rb.setObj(resultado);
            }
            else{
                rb.setErrorCode(-5);
            }
        }
        catch (NumberFormatException e) {
            rb.setErrorCode(-3);
            e.printStackTrace();
        }
        catch (HibernateException e) {
            rb.setErrorCode(-4);
            e.printStackTrace();
        }
        catch (Excepciones e) {
            rb.setErrorCode(e.getCodigo());
        }
        catch (Exception e) {
            rb.setErrorCode(-5);
            e.printStackTrace();
        }
           
        
        return getJson().toJson(rb);
    }
}
