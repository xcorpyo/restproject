package com.compitruenos.restproject.ws;

import java.util.Set;
import javax.ws.rs.core.Application;

@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.compitruenos.restproject.ws.InmobiliariaWS.class);
        resources.add(com.compitruenos.restproject.ws.ServicesWS.class);
        resources.add(com.compitruenos.restproject.ws.ServicioWifiWS.class);
        resources.add(com.compitruenos.restproject.ws.TiempoWS.class);
        resources.add(com.compitruenos.restproject.ws.UserWS.class);
        resources.add(com.compitruenos.restproject.ws.ViajesWS.class);
    }
    
}
