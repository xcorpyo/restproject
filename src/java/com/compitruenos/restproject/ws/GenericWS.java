package com.compitruenos.restproject.ws;

import com.compitruenos.restproject.util.HibernateUtil;
import com.google.gson.Gson;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class GenericWS {
    
    private Session s;
    private Transaction tx;
    private Gson json;
    
    public GenericWS () { 
        this.json = new Gson();
    }
    
    public void connect () throws HibernateException, Exception{
        s = HibernateUtil.getSessionFactory().getCurrentSession();
        tx = s.beginTransaction();
    }
    
    public void disconnect () throws HibernateException, Exception{
        if (tx!=null && tx.isActive()) tx.commit();
    }

    public Gson getJson() {
        return json;
    }

    public Session getS() {
        return s;
    }

    public Transaction getTx() {
        return tx;
    }
    
}