package com.compitruenos.restproject.ws;

import com.compitruenos.restproject.controller.CheckIP;
import com.compitruenos.restproject.controller.CheckToken;
import com.compitruenos.restproject.controller.Excepciones;
import com.compitruenos.restproject.dao.TiempoDAO;
import com.compitruenos.restproject.rb.ResponseBean;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.hibernate.HibernateException;

@Path("tiempo")
public class TiempoWS extends GenericWS{

    @Context
    //private UriInfo context;
    private javax.servlet.http.HttpServletRequest hsr;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String localizar(@QueryParam("ciudad") String ciudad, @QueryParam("zipcode") String zipCode, @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud, @QueryParam("token") String token){
        ResponseBean rb = new ResponseBean();
        try {
            if(token == null || token.trim().isEmpty())
                throw new Excepciones(-1);
            if(!new CheckToken().checkToken(token))
                throw new Excepciones(-22);
            if(!CheckIP.checkIP(hsr))
                throw new Excepciones(-16);
            
            TiempoDAO tDAO = new TiempoDAO(getS(), getTx());
            if(ciudad != null && zipCode == null && latitud == null && longitud == null){
                if(ciudad.trim().isEmpty())
                    rb.setErrorCode(-5);
                else{
                    String fin = tDAO.locCiudad(ciudad);
                    if(fin == null)
                        rb.setErrorCode(-1);
                    else{
                        rb.setErrorCode(0);
                        rb.setObj(fin);
                    }  
                }
            }
            else if(ciudad == null && zipCode != null && latitud == null && longitud == null){
                if(zipCode.trim().isEmpty())
                    rb.setErrorCode(-5);
                else{
                    String fin = tDAO.locZip(zipCode);
                    if(fin == null)
                        rb.setErrorCode(-1);
                    else{
                        rb.setErrorCode(0);
                        rb.setObj(fin);
                    }
                }
            }
            else if(ciudad == null && zipCode == null && latitud != null && longitud != null){
                if(latitud.trim().isEmpty() || longitud.trim().isEmpty())
                    rb.setErrorCode(-5);
                else{
                    double lat = Double.parseDouble(latitud);
                    double lon = Double.parseDouble(longitud);
                    String fin = tDAO.locLatiLon(lat, lon);
                    if(fin == null)
                        rb.setErrorCode(-1);
                    else{
                        rb.setErrorCode(0);
                        rb.setObj(fin);
                    }
                }
            }
            else
                rb.setErrorCode(-5);
        }
        catch (NumberFormatException e) {
            rb.setErrorCode(-3);
            e.printStackTrace();
        }
        catch (HibernateException e) {
            rb.setErrorCode(-4);
            e.printStackTrace();
        }
        catch (Excepciones e) {
            rb.setErrorCode(e.getCodigo());
        }
        catch (Exception e) {
            rb.setErrorCode(-5);
            e.printStackTrace();
        }
        
        return getJson().toJson(rb);
    }
}
