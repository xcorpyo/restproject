package com.compitruenos.restproject.ws;

import com.compitruenos.restproject.controller.CheckIP;
import com.compitruenos.restproject.controller.CheckToken;
import com.compitruenos.restproject.controller.Excepciones;
import com.compitruenos.restproject.dao.ViajesDAO;
import com.compitruenos.restproject.rb.ResponseBean;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.hibernate.HibernateException;

@Path("viajes")
public class ViajesWS extends GenericWS {

    @Context
    //private UriInfo context;
    private javax.servlet.http.HttpServletRequest hsr;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String calcular(@QueryParam("origen") String origen, @QueryParam("destino") String destino, @QueryParam("token") String token) {
        ResponseBean rb = new ResponseBean();
        try{
            if(token == null || token.trim().isEmpty())
                throw new Excepciones(-1);
            if(!new CheckToken().checkToken(token))
                throw new Excepciones(-22);
            if(!CheckIP.checkIP(hsr))
                throw new Excepciones(-16);
        
            if(origen == null || origen.trim().isEmpty() || destino == null || destino.trim().isEmpty())//COMPROBAMOS QUE VENGAN BIEN LAS VARIABLES ENTERAS
                throw new Excepciones(-1);
            
            ViajesDAO vDAO = new ViajesDAO(getS(), getTx());
            
            Map<String, String> fin = vDAO.calcularDirecion(origen, destino);
            if(fin != null){
                rb.setErrorCode(0);
                rb.setObj(fin);
            }
            else{
                rb.setErrorCode(-5);
            }
        }
        catch (NumberFormatException e) {
            rb.setErrorCode(-3);
        }
        catch (HibernateException e) {
            rb.setErrorCode(-4);
            e.printStackTrace();
        }
        catch (Excepciones e) {
            rb.setErrorCode(e.getCodigo());
        }
        catch (Exception e) {
            rb.setErrorCode(-5);
            e.printStackTrace();
        }
        
        return getJson().toJson(rb);
    }
}
