package com.compitruenos.restproject.pojo;
// Generated 15-feb-2019 0:50:06 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Services generated by hbm2java
 */
@Entity
@Table(name="services"
    ,catalog="projectrest"
    , uniqueConstraints = @UniqueConstraint(columnNames="nameServices") 
)
public class Services  implements java.io.Serializable {


     private Integer idServices;
     private String nameServices;
     private Date dateRegister;
     private Date dateEnd;
     private int permission;
     private Set<ServicesHasUser> servicesHasUsers = new HashSet<ServicesHasUser>(0);

    public Services() {
    }

	
    public Services(String nameServices, Date dateRegister, int permission) {
        this.nameServices = nameServices;
        this.dateRegister = dateRegister;
        this.permission = permission;
    }
    public Services(String nameServices, Date dateRegister, Date dateEnd, int permission, Set<ServicesHasUser> servicesHasUsers) {
       this.nameServices = nameServices;
       this.dateRegister = dateRegister;
       this.dateEnd = dateEnd;
       this.permission = permission;
       this.servicesHasUsers = servicesHasUsers;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="idServices", unique=true, nullable=false)
    public Integer getIdServices() {
        return this.idServices;
    }
    
    public void setIdServices(Integer idServices) {
        this.idServices = idServices;
    }

    
    @Column(name="nameServices", unique=true, nullable=false, length=45)
    public String getNameServices() {
        return this.nameServices;
    }
    
    public void setNameServices(String nameServices) {
        this.nameServices = nameServices;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dateRegister", nullable=false, length=19)
    public Date getDateRegister() {
        return this.dateRegister;
    }
    
    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dateEnd", length=19)
    public Date getDateEnd() {
        return this.dateEnd;
    }
    
    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    
    @Column(name="permission", nullable=false)
    public int getPermission() {
        return this.permission;
    }
    
    public void setPermission(int permission) {
        this.permission = permission;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="services")
    public Set<ServicesHasUser> getServicesHasUsers() {
        return this.servicesHasUsers;
    }
    
    public void setServicesHasUsers(Set<ServicesHasUser> servicesHasUsers) {
        this.servicesHasUsers = servicesHasUsers;
    }




}


