FALLOS  
  
 5		-> Servicio enlazado correctamente  
 4		-> Servicio añadido correctamente  
 3		-> Usuario correcto  
 2		-> Usuario registrado correctamente  
 1   	-> Servicio acesible  
   
 0   	-> OK  
   
-1  	-> Faltan Parametros  
-2  	-> Usuario Existente  
-3  	-> Parametros en formato incorrecto  
-4  	-> Error Hibernate  
-5  	-> Error generico  
-6  	-> Usuario o Contraseña incorrectos  
-7 	-> Servicio inacesible  
-8 	-> Fallo al registrar  
-9  	-> Enlace ya existente  
-10 	-> Token Inexistente  
-11	-> Imposible realizar conexion  
-12	-> Servicio ya existente  
-13	-> Fallo al linkear servicios  
-14	-> Ningun servicio o usuario disponible para linkear  
-15	-> Fallo al añadir servicio
-16	-> Cliente es distinta de localhost  
-17	-> Distrito no existente  
-18 -> Error durante la recogida de datos  
-19	-> No se encuentra el elemento selecionado  
-20	-> Longitud de elementos y variables diferente  
-21	-> Parametro en elemento repetido
-22	-> Token invalido o caducado